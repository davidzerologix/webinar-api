import express from "express";
import AuthMiddleware from "../middleware/AuthMiddleware";
import IdValidMiddleware from "../middleware/IdValidMiddleware";
import FavouriteModel from "../model/FavouriteModel";
import WebinarModal from "../model/WebinarModal";

const FavouritesRouter = express.Router();

FavouritesRouter.post("/posts/:id", AuthMiddleware, IdValidMiddleware, (req, res) => {
  const favouriteId = req.params.id;

  if(FavouriteModel.isIdExist(favouriteId)) {
    return res.status(403).send('Webinar already registered');  
  }

  if(!WebinarModal.isIdExist(favouriteId)) {
    return res.status(404).send('Webinar not found');  
  }

  FavouriteModel.addId(favouriteId);

  return res.status(200).send("Registered");
});

FavouritesRouter.delete("/posts/:id", AuthMiddleware, IdValidMiddleware, (req, res) => {
  const favouriteId = req.params.id;
  
  if(!FavouriteModel.isIdExist(favouriteId)) {
    return res.status(403).send('Webinar not registered');  
  }

  if(!WebinarModal.isIdExist(favouriteId)) {
    return res.status(404).send('Webinar not found');  
  }

  FavouriteModel.removeId(favouriteId);

  return res.status(200).send("Unregistered");
});

export default FavouritesRouter;