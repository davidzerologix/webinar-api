import { RequestHandler } from "express";
import TokenModel from "../model/TokenModel";

const AuthMiddleware: RequestHandler = (req, res, next) => {
  if(!req.headers.authorization) {
    return res.status(401).send('Unauthorized');
  }
  const token = req.headers.authorization.replace('Bearer ', '');
  if(!TokenModel.isTokenValid(token)) {
    return res.status(401).send('Unauthorized');
  }
  next()
}

export default AuthMiddleware;