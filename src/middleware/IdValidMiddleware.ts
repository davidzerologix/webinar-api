import { RequestHandler } from "express";

const IdValidMiddleware: RequestHandler = (req, res, next) => {
  if(!req.params.id) {
    return res.status(400).send('Id is required');
  }

  const id  = Number(req.params.id);

  if(Number.isNaN(id) || !Number.isInteger(id) || id < 0) {
    return res.status(400).send('Id is invalid');
  }

  next()
}

export default IdValidMiddleware;