import { readFileSync } from 'fs';
import path from "path";
import FavouriteModel from './FavouriteModel';

class WebinarModal {
  static DATA_FILE_PATH = path.resolve(__dirname, '../data/webinarList.json');
  static DEFAULT_PER_PAGE = 12;

  static getWebinarList(page: number, perPage: number = WebinarModal.DEFAULT_PER_PAGE) {
    try {
      const rawData = readFileSync(WebinarModal.DATA_FILE_PATH);
      const rawJson = JSON.parse(rawData.toString('utf8'));
      const webinarList = rawJson.data;
      const startIndex = perPage * (page - 1);
      const endIndex = startIndex + perPage;
      const data = webinarList.slice(startIndex, endIndex);
      return {
        data,
        meta: {
          pagination: {
            total: webinarList.length,
            count: data.length,
            per_page: perPage,
            current_page: page,
            total_pages: Math.ceil(webinarList.length / perPage),
          }
        }
      }
    } catch (error) {
      throw new Error('data error');
    }
  }
  static getFavouritedWebinarList() {
    try {
      const rawData = readFileSync(WebinarModal.DATA_FILE_PATH);
      const rawJson = JSON.parse(rawData.toString('utf8'));
      const webinarList = rawJson.data;
      const favouritIdList = FavouriteModel.getFavouriteIdList();
      const favouritedWebinars =  webinarList.filter((webinar: any) => favouritIdList.includes(webinar.id)).map((webinar: any) => ({...webinar, favourited: true}))
      return {
        data: favouritedWebinars
      }
    } catch (error) {
      throw new Error('data error');
    }
  }

  static isIdExist(id: string): boolean {
    try {
      const rawData = readFileSync(WebinarModal.DATA_FILE_PATH);
      const rawJson = JSON.parse(rawData.toString('utf8'));
      return rawJson.data.some((webinar: any)=> webinar.id == id)
    } catch (error) {
      return false;
    }
  }
}

export default WebinarModal