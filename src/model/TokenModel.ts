import jsonwebtoken, { JwtPayload } from "jsonwebtoken";

class TokenModel {
  static PRIVATE_KEY = 'IAMDAVID';
  static MESSAGE = 'David is cool';
  static TOKEN_LIFETIME = "1h";

  static generateToken(): string {
    const token  = jsonwebtoken.sign(
      {
        message: TokenModel.MESSAGE
      },
      TokenModel.PRIVATE_KEY,
      {
        expiresIn: TokenModel.TOKEN_LIFETIME
      }
    );

    return token;
  }

  static isTokenValid(token: string): boolean {
    try {
      const decoded = jsonwebtoken.verify(token, TokenModel.PRIVATE_KEY) as JwtPayload;
      return decoded.message === TokenModel.MESSAGE;
    } catch (error) {
      console.log(error)
      return false;
    }
  }
}

export default TokenModel;